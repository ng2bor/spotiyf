import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'

@Injectable()
export class SpotifyService {

	constructor(private _http: Http) { }

	// TODO: create methods for POST
	post() {
		console.log('-> PASS POST')
	}
	
	// MARK: create methods for GET
	
	get(q: string): Observable<any> {
		return this._http.get('https://api.spotify.com/v1/search?type=artist&q=' + q)
		.map(this.extracData)
		.catch(this.handleError)
	}

	// TODO: create methods for DELETE
	// TODO: create methods for PUT

	private extracData = (res: Response) => {

		if (res.status < 200 || res.status >= 300) {
			throw new Error('Bad status ' + res.status)
		}

		let body = res.json()

		if (body.success == false) {
			throw new Error(body.message)
		}
		return body || {}
	}

	private handleError = (error: any) => {
		var message = error._body ? JSON.parse(error._body) : error.message
		return Observable.throw(message)
	}



}



exports class Students {

    constructor() {
        var r = this.myMethod(1, 2, 3)
        this.dbug('ja sam dbug tekst!')
    }

    myMethod(a: number, b: number, c: number): number {
        var result = a + b + c
        return result
    }

    dbug(text: String) {
        console.log(text)
    }

}
import { SpotifyService } from './spotify.service'
import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {

	constructor(private _spotify: SpotifyService) { 
		var r = this.myMethod(1, 2, 3)
        this.dbug('ja sam dbug tekst!')
	}

	searchResults = []

	doSearch() {

		this._spotify.get('Iron').subscribe((data) => {
			this.searchResults = data
		}, (error) => {

		})

		//console.log('-> PASS from component')
	}


	myMethod(a: number, b: number, c: number): number {
        var result = a + b + c
        this.dbug('rezultat:' + result)
        return result
    }

    dbug(text: String) {
        console.log(text)
    }









}
